<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Validator;

class TodosController extends Controller
{
    /**
     * Display a listing of the user's todos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userTodos = Todo::where('user_id',$request->user()->id)->get();

        return response()->json($userTodos);
    }

    /**
     * Store a newly created todo in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'text' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'text field in the body is required'
            ], 401);
        }

        $todo = Todo::create([
            'text' =>  $request->input('text'),
            'completed' => 0,
            'user_id' => $request->user()->id
        ]);

        return response()->json($todo);
    }

    /**
     * Retrieve the user's todo by ID
     *
     * @param  integer  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        //get the user's todo from the db
        $todo = $this->_getUserTodo($request, $id);

        if(empty($todo)){
            return response()->json([
                'message' => 'Todo not found'
            ], 404);
        }

        return response()->json($todo);
    }


    /**
     * Update the user's todo
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * either the text or completed field must be required in the
         * request body
         */
        $validator = Validator::make($request->all(), [
            'text' => 'required_without:completed|string',
            'completed' => 'required_without:text|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first()
            ], 401);
        }

        $todo = $this->_getUserTodo($request, $id);

        if(empty($todo)){
            return response()->json([
                'message' => 'Todo not found'
            ], 404);
        }


        $reqParams = request(['text','completed']);


        if(!empty($reqParams['text'])){
            $todo->text = $reqParams['text'];
        }

        if(isset($reqParams['completed'])){
            if(!$reqParams['completed']){
                $todo->completed = 0;
            }else{
                $todo->completed = 1;
            }
        }

        $todo->save();

        return response()->json($todo);
    }

    /**
     * Delete the todo from the db
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id,Request $request)
    {
        //get the user's todo from the db
        $todo = $this->_getUserTodo($request, $id);

        if(empty($todo)){
            return response()->json([
                'message' => 'Todo not found'
            ], 404);
        }

        $result = $todo->delete();

        if($result){
            return response()->json(['message'=> 'Todo deleted.']);
        }


        return response()->json([
            'message' => 'Unable to delete todo.'
        ], 401);
    }


    /**
     * Get user's todo from db
     *
     * @param  integer  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Todo
     */
    protected function _getUserTodo(Request $request, $id)
    {
        if(empty($id) || !is_numeric($id)){
            return response()->json([
                'message' => 'Invalid todo ID. ID must be an integer'
            ], 401);
        }

        $todo = Todo::where('id',$id)
            ->where('user_id',$request->user()->id)
            ->first();

        return $todo;
    }
}
