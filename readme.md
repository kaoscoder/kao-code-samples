## About this application

 A simple TODO API with user authentication on [Laravel](https://laravel.com/docs).
This application was implemented by Kao Saelee and used as a sample code for potential jobs he'd applied to.

## Server Requirements:

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension

## How to install

Create a new MySQL database then edit the .env file and update the database connection information.

Then open up the your terminal and CD into the root directory and run "composer install".

If the database tables did were not created during installation, please run "php artisan migrate"


## Files to check out:

- /routes/api.php - contains the routes to the TODO API end points
- /app/Http/Controllers/ApiAuthController.php - Handles API Authentication
- /app/Http/Controllers/TodosController.php - Handles TODO APIs
- /app/Todo.php - Todo Eloquent Model
- /app/User.php - User Eloquent Model