<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Routes for authentication
 */
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'ApiAuthController@login');
    Route::post('user', 'ApiAuthController@addUser');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'ApiAuthController@logout');
        Route::get('user', 'ApiAuthController@getUser');
    });
});

Route::middleware(['auth:api'])->group(function () {

    /**
     * handles routes for GET, POST, PATCH and DELETE
     */
    Route::apiResource('todos', 'TodosController');
});
